package com.example.activityandlayouts

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "TestApp"
        button.setOnClickListener() {
            val intent = Intent(this, InfoActivity::class.java)
            intent.putExtra(Conctans.USERNAME, editTextSurname.text.toString())
            startActivity(intent)
        }
        LoadData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.save_data -> {
                SaveData()
            }
            R.id.next_bg -> {
                var x: Int = (1..3).random()
                when(x) {
                    1 -> MainLayout.setBackgroundResource(R.drawable.background1)
                    2 -> MainLayout.setBackgroundResource(R.drawable.background2)
                    3 -> MainLayout.setBackgroundResource(R.drawable.background3)
                }
            }
            R.id.other_act -> {
                val intent = Intent(this, OtherActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }

    private fun SaveData() {
        val Surname = editTextSurname.text.toString()
        editTextSurname.hint = Surname
        val Name = editTextName.text.toString()
        editTextName.hint = Name
        val Patronymic = editTextPatronymic.text.toString()
        editTextPatronymic.hint = Patronymic
        val Age = editTextAge.text.toString()
        editTextAge.hint = Age
        val Hobby = editTextHobby.text.toString()
        editTextHobby.hint = Hobby

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply() {
            putString("SURNAME", Surname)
            putString("NAME", Name)
            putString("PATRONYMIC", Patronymic)
            putString("AGE", Age)
            putString("HOBBY", Hobby)
        }.apply()

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
    }

    private fun LoadData() {
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val Surname = sharedPreferences.getString("SURNAME", "Фамилия")
        val Name = sharedPreferences.getString("NAME", "Имя")
        val Patronymic = sharedPreferences.getString("PATRONYMIC", "Отчество")
        val Age = sharedPreferences.getString("AGE", "Возраст")
        val Hobby = sharedPreferences.getString("HOBBY", "Хобби")

        editTextSurname.hint = Surname
        editTextName.hint = Name
        editTextPatronymic.hint = Patronymic
        editTextAge.hint = Age
        editTextHobby.hint = Hobby
    }
}