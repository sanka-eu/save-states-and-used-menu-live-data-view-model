package com.example.activityandlayouts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_other.*

class OtherActivity : AppCompatActivity() {
    lateinit var viewModel: ViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)

        viewModel.currentNumber.observe(this, Observer {
             count.text = it.toString()
        })

        incrementText()
    }

    private fun incrementText() {
        other_btn.setOnClickListener() {
            viewModel.currentNumber.value = ++viewModel.count
        }
    }
}
