package com.example.activityandlayouts

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val username = intent.getStringExtra(Conctans.USERNAME)
        textView.text = "Добро пожаловать $username"
    }
}